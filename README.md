# How to Change Password on Ubuntu/Linux Mint (from Command Line)

Open a terminal.

    passwd

You will be prompted to enter your old password for verification, and then a new password.

If you want to change somebody else's password,

    sudo passwd _target username_

